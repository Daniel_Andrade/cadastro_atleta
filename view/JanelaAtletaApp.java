package view;

import java.awt.BorderLayout;
import model.Atleta;
import controller.ControladorAtleta;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;

public class JanelaAtletaApp extends JFrame{
	
	private JPanel contentPane;
	private JTextField campoNome;
	private JTextField campoIdade;
	private JTextField campoPeso;
	private final Action CadastrarAction = new SwingAction();
	private Atleta modelo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JanelaAtletaApp frame = new JanelaAtletaApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JanelaAtletaApp() {
		modelo = new Atleta();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 548, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		campoNome = new JTextField();
		campoNome.setBounds(129, 40, 114, 19);
		contentPane.add(campoNome);
		campoNome.setColumns(10);
		
		campoIdade = new JTextField();
		campoIdade.setBounds(129, 97, 114, 19);
		contentPane.add(campoIdade);
		campoIdade.setColumns(10);
		
		campoPeso = new JTextField();
		campoPeso.setBounds(129, 71, 114, 19);
		contentPane.add(campoPeso);
		campoPeso.setColumns(10);
		
		JLabel lblNomase = new JLabel("Nome:");
		lblNomase.setBounds(41, 42, 70, 15);
		contentPane.add(lblNomase);
		
		JLabel lblIdade_1 = new JLabel("Idade:");
		lblIdade_1.setBounds(41, 99, 70, 15);
		contentPane.add(lblIdade_1);
		
		JButton btnCadastrar = new JButton("CADASTRAR");
		btnCadastrar.setAction(CadastrarAction);
		btnCadastrar.setBounds(136, 201, 117, 25);
		contentPane.add(btnCadastrar);
		
		JLabel lblAltura = new JLabel("Peso:");
		lblAltura.setBounds(41, 71, 70, 15);
		contentPane.add(lblAltura);
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Cadastrar");
			putValue(SHORT_DESCRIPTION, "Cadastrar o Atleta");
		}
		public void actionPerformed(ActionEvent e) {
			String nome = campoNome.getText();
			String peso = campoPeso.getText();
			String idade = campoIdade.getText();
			modelo.setNome(nome);
			modelo.setPeso(peso);
			modelo.setIdade(idade);
			
			ControladorAtleta umControlador = new ControladorAtleta();
			
			umControlador.adicionar(modelo);
			
			
		}
	}
}
