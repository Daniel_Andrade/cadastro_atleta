package view;

import model.Atleta;
import model.Endereco;
import controller.ControladorAtleta;
import java.util.Scanner;

public class TelaAtleta {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int opcao = -1;
		// Instanciando...
		Scanner leitor = new Scanner(System.in);
		Atleta umAtleta = null;
		Endereco umEndereco = null;
		ControladorAtleta umControlador = new ControladorAtleta();
		
		while(opcao != 0){
			System.out.println("Cadastro Atleta \n"
					+ "1 - Adicionar Atleta\n"
					+ "2 - Remover Atleta\n"
					+ "3 - Listar Atleta\n"
					+ "0 - Fechar Programa");
			
			opcao = leitor.nextInt();
			
			switch(opcao){
			
			case 1:
				// Adicionando Atleta
				// Instanciando o Atleta
				System.out.println("Informe o nome do Atleta:");
				String nome = leitor.nextLine();
				
				umAtleta = new Atleta(nome);
				
				System.out.println("Informe a idade do Atleta:");
				String idade = leitor.next();
				
				umAtleta.setIdade(idade);
				
				System.out.println("Informe o peso do Atleta:");
				String peso = leitor.next();
				
				umAtleta.setPeso(peso);
				
				// Instanciando Endereço
				System.out.println(">> ENDERECO");
				
				System.out.println("Informe o estado(uf):");
				String estado = leitor.next();
				
				umEndereco = new Endereco(estado);
				
				System.out.println("Informe o logradouro:");
				String logradouro = leitor.next();
				
				umEndereco.setLogradouro(logradouro);
				
				System.out.println("Informe o numero:");
				String numero = leitor.next();
				
				umEndereco.setNumero(numero);
				
				System.out.println("Informe o CEP:");
				String cep = leitor.next();
				
				umEndereco.setCep(cep);
				
				System.out.println("Informe a cidade:");
				String cidade = leitor.next();
				
				umEndereco.setCidade(cidade);
				
				// Passando Endereco para Atleta.
				umAtleta.setEndereco(umEndereco);
				
				
				// Agora passando todo o Atleta para o Controlador
				
				umControlador.adicionar(umAtleta);
			
			break;
			case 2:
				//Remover Atletas
				umControlador.mostra();
				System.out.println("\nDigite o Nome do Atleta: ");
				
				umAtleta = umControlador.buscar(leitor.next());
				umControlador.remover(umAtleta);
				
			break;
			
			case 3:
				// Listar Atletas
				umControlador.mostra();
				System.out.println("\nDigite o Nome do Atleta: ");
				
				umAtleta = umControlador.buscar(leitor.next());
				System.out.println("Nome: " + umAtleta.getNome());
				System.out.println("Idade: " + umAtleta.getIdade());
				System.out.println("Peso: " + umAtleta.getPeso());
				System.out.println("Logradouro: " + umEndereco.getLogradouro());
				System.out.println("Estado: " + umEndereco.getEstado());
				System.out.println("CEP: " + umEndereco.getCep());
				System.out.println("Numero: " + umEndereco.getNumero());
				
				
			break;
			
			default:
				//System.out.println("Opção Inválida!");
			break;
			
			}
			leitor.close();
		}
		System.out.println("Fechando o programa! ");
	}
	
		
}
