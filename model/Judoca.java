package model;

public class Judoca extends Atleta{
	
	private String faixa;
	private String Categoria;
	private int bomSolo;
	
	public Judoca(String nome, String faixa) {
		super(nome);
		this.faixa = faixa;
	}


	public String getFaixa() {
		return faixa;
	}


	public void setFaixa(String faixa) {
		this.faixa = faixa;
	}


	public String getCategoria() {
		return Categoria;
	}


	public void setCategoria(String categoria) {
		Categoria = categoria;
	}


	public int getBomSolo() {
		return bomSolo;
	}


	public void setBomSolo(int bomSolo) {
		this.bomSolo = bomSolo;
	}

}
